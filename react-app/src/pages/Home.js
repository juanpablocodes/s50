import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Courses from '../components/CourseCard';
// import Resources from '../components/Resources'

export default function Home() {

    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <Fragment>
            <Banner data={data}/>
            <Highlights/>
             {/*<CourseCard/>*/}
            {/*<Resources/>*/}
        </Fragment>
    )
};