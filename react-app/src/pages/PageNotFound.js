import Banner from '../components/Banner';
  
export default function PageNotFound() {

	const data = {
		title: "Error 404 - Page Not Found",
		content: "The Page you are looking cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

    return (
    	<Banner data={data} />
    )
}
