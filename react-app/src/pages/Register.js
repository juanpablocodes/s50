import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	

	const [isActive, setIsActive] = useState(false);

	// console.log(firstName);
	// console.log(lastName);
	// console.log(mobileNo);
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			if(data){
				Swal.fire({
					title: "Duplicate Email",
					icon: "error",
					text: "Your email is already exist"
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo

				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

			if(data) {
				Swal.fire({
					title: "You are successfully registered!",
					icon: "success",
					text: "Please log in."
				})
			}

			navigate('/login')

			// Clear input fields
			setFirstName("");
			setLastName("")
			setEmail("");
			setMobileNo("");
			})
		}	
	})
}

	useEffect(() => {
		if (firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo])

	return(

		(user.id !== null) ?
			<Navigate to="/login"/>
			:
			<Form onSubmit={(e) => registerUser(e)} >

				  <h1 className="text-center my-3">Registration</h1>

				  <Form.Group className="mb-3" controlId="firstName">
				    <Form.Label>First Name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={firstName}
				    	onChange={(e) => {setFirstName(e.target.value)}}
				    	placeholder="Enter your First Name" 
				    	required
				    	/>
				  </Form.Group>

				  <Form.Group className="mb-3" controlId="lastName">
				    <Form.Label>Last Name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={lastName}
				    	onChange={(e) => {setLastName(e.target.value)}}
				    	placeholder="Enter your Last Name" />
				  </Form.Group>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value={mobileNo}
			        	onChange={(e) => {setMobileNo(e.target.value)}}
			        	placeholder="0999999999" />
			      </Form.Group>

			      { isActive ?
			      			<Button variant="primary" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      			:
			      			<Button variant="primary" type="submit" id="submitBtn" disabled>
			      			  Submit
			      			</Button>
			      }  
			    </Form>	

	)
}
