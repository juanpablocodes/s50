import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';

function showExample() {
  return (
    <Accordion>
      <Accordion.Item eventKey="0" className = "p-1 mt-3">
        <Accordion.Header>React Tutorial</Accordion.Header>
        <Accordion.Body>
          This tutorial is designed for people who prefer to learn by doing. If you prefer learning concepts from the ground up, check out our step-by-step guide. You might find this tutorial and the guide complementary to each other.
          <a href="https://reactjs.org/tutorial/tutorial.html" target="_blank">
          <Button variant="primary" size="sm">
          Read more
          </Button></a>
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="1" className = "p-1 mt-3">
        <Accordion.Header>React Vs Vue: A Battle of Two Most Popular Front-end JS Frameworks</Accordion.Header>
        <Accordion.Body>
          It is evidently clear that JavaScript has evolved to become one of the most popular and widely utilized frameworks for creating web applications. As time is progressing, the developers are getting access to more front-end development tools, which often make the selection process trickier. However, quite manifestly so, the choice needs to be made on the basis of the project requirements and the expertise of the development team.
          <a href="https://www.mindinventory.com/blog/reactjs-vs-vuejs/" target="_blank">
          <Button variant="primary" size="sm">
          Read More
          </Button></a>
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="2" className = "p-1 mt-3">
        <Accordion.Header>How to Upgrade to React 18</Accordion.Header>
        <Accordion.Body>
          React 18 introduces a new root API which provides better ergonomics for managing roots. The new root API also enables the new concurrent renderer, which allows you to opt-into concurrent features.
          <a href="https://reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html" target="_blank">
          <Button variant="primary" size="sm">
          Read More
          </Button></a>
        </Accordion.Body> 
      </Accordion.Item>
    </Accordion>
  );
}

export default showExample;
